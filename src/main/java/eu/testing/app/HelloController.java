package eu.testing.app;

import eu.testing.app.util.Helper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("default")
public class HelloController {

  @Value("${server.port}")
  private String port;

  @RequestMapping("/")
  public String page0() {
    String temp = "HELLLO Greetings from Spring Boot!Path: /test ON port:" + port + " and ip ---> " + Helper.getPrivateIp();
    return temp;
  }


  @RequestMapping("/test")
  public String page1() {
    String temp = "Greetings from Spring Boot!Path: /test ON port:" + port + " and ip ---> " + Helper.getPrivateIp();
    return temp;
  }

  @RequestMapping("/v1/api/actions")
  public String page2() {
    String temp = "actions from Spring Boot!Path: /v1/api/actions ON port:" + port + " and ip ---> " + Helper.getPrivateIp();
    return temp;
  }

}