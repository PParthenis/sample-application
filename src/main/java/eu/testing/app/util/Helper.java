package eu.testing.app.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.regex.Pattern;

/**
 * @author Panagiotis Parthenis
 */
public class Helper {

  public static String getPrivateIp() {
    String privateIP = "...error...";
    String ipv4Pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
    Pattern VALID_IPV4_PATTERN = Pattern.compile(ipv4Pattern, Pattern.CASE_INSENSITIVE);

    Enumeration<NetworkInterface> nics = null;
    try {
      nics = NetworkInterface.getNetworkInterfaces();
    } catch (SocketException e) {
    }
    while (nics.hasMoreElements()) {
      NetworkInterface nic = nics.nextElement();
      Enumeration<InetAddress> addrs = nic.getInetAddresses();
      while (addrs.hasMoreElements()) {
        InetAddress addr = addrs.nextElement();
        if (VALID_IPV4_PATTERN.matcher(addr.getHostAddress()).matches() && (addr.getHostAddress().compareTo("127.0.0.1") != 0) && (addr.getHostAddress().compareTo("172.17.0.1") != 0)) {
          privateIP = addr.getHostAddress();
        }
      }
    }
    return privateIP;
  }//EoM
}
