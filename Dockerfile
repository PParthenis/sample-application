FROM publicrepository.ubitech.eu/generic-jdk:0.1.0

ADD ./target/sample-application-0.1.0.jar /app/

RUN cjdroute --genconf >> cjdroute.conf && cjdroute < cjdroute.conf > cjdroute.log

CMD java -jar /app/sample-application-0.1.0.jar
